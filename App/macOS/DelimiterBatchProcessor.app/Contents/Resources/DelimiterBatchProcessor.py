# Matheus Vilano
# May 16th, 2021

# Modules

import os
import webbrowser
import platform

import tkinter as tk # The variable "tk" acts as a nickname for tkinter
from tkinter import filedialog
from tkinter.constants import CENTER, END, LEFT # To handle directory browsing

# Initialization (create the Graphical User Interface)

app = tk.Tk() # This creates the GUI app
app.resizable(False, False)
app.title("Batch Text Processor")
app.tk.call('tk', 'scaling', 1.5)
app.eval('tk::PlaceWindow . center')

# Tkinter Objects

infoLabel = None
repoButton = None

directoryLabel = None
directoryEntry = None
directoryBrowseButton = None

wildcardsLabel = None
wildcardsEntry = None
wildcardsAddButton = None
wildcardsRemoveButton = None
wildcardsSetLabel = None

capitalizationCheckbox = None

renameFilesButton = None

consoleLogLabel = None

# Style Varibles

borderSize = 2
paddingSize = 5

backgroundColour = "aliceblue"      if platform.system() == "Windows" else "white"
foregroundColour = "black"          if platform.system() == "Windows" else "black" # Just in case the foreground is changed.
backgroundAltColour = "darkblue"    if platform.system() == "Windows" else "white"
foregroundAltColour = "white"       if platform.system() == "Windows" else "black"

# Core Variables

rootFolder = ""
files = []

wildcards = []
wildcardText = tk.StringVar()

willCapitalize = tk.BooleanVar(value=True)

# Functions

def browseDirectory(): # This will handle selecting a directory and saving its path.
    global rootFolder
    global files
    rootFolder = filedialog.askdirectory()
    if rootFolder != "" and rootFolder != None: # Erroc checking: make sure the path is valid.
        directoryEntry.delete(0, END) # Clear text (entry) box
        directoryEntry.insert(0, rootFolder) # Show directory in the text (entry) box

def onWildcardShortcutPressed(event):
    if event.keysym == "Return":
        addWildcard()
    elif event.keysym == "Escape":
        removeWildcard()

def addWildcard():
    wildcard = wildcardsEntry.get()
    if wildcard == '.':
        consoleLogLabel["text"] = "The char '.\' is not supported."
        consoleLogLabel["foreground"] = "red"
    elif str(wildcard).isalpha():
        consoleLogLabel["text"] = "Letters (alpha) are not supported."
        consoleLogLabel["foreground"] = "red"
    elif wildcard != "" and wildcard != None and wildcard not in wildcards and wildcard != '_': # Error checking
        wildcards.append(wildcardsEntry.get()[0])
        consoleLogLabel["text"] = "Added wildcard to set."
        consoleLogLabel["foreground"] = "green"
        updateWildcards()
    else:
        consoleLogLabel["text"] = "No wildcard was added."
        consoleLogLabel["foreground"] = "red"
    wildcardsEntry.delete(0, END)

def removeWildcard():
    wildcard = wildcardsEntry.get()
    if wildcard != "" and wildcard != None and wildcard in wildcards and wildcard != '_': # Error checking
        wildcards.remove(wildcard)
        consoleLogLabel["text"] = "Removed wildcard from set."
        consoleLogLabel["foreground"] = "green"
        updateWildcards()
    else:
        consoleLogLabel["text"] = "No wildcard was removed."
        consoleLogLabel["foreground"] = "red"
    wildcardsEntry.delete(0, END)

def updateWildcards():
    global wildcards
    if len(wildcards) == 0: # If empty, do not display anything
        wildcardsSetLabel["text"] = "No wildcards."
        wildcardsSetLabel["foreground"] = "blue"
    else:
        wildcardsFormatted = "" # Temporarily (see below)
        for wildcard in wildcards:
            wildcardsFormatted += (wildcard + ' ') if (wildcard != ' ') else ("space ");
        wildcardsSetLabel["text"] = wildcardsFormatted

def checkWildcardCharacterCount():
    if len(wildcardText.get()) > 0:
        wildcardText.set(wildcardText.get()[-1])

def renameFiles():
    global rootFolder
    global files
    files = os.listdir(rootFolder)
    if len(files) <= 0: # If empty, let the user know
        consoleLogLabel["text"] = "No files to rename."
        consoleLogLabel["foreground"] = "red"
    else:
        try:
            consoleLogLabel["text"] = "Renaming. This might take some time."
            consoleLogLabel["foreground"] = "blue"
            for path, subdirs, files in os.walk(rootFolder):
                for originalFileName in files:
                    newFileName = originalFileName # Temporarily (will change below)
                    for wildcard in wildcards:
                        newFileName = newFileName.replace(wildcard, '_').replace("___", '_').replace("__", '_') # Avoid duplicated characters
                    tokens = newFileName.split('_') # Separate all words
                    if willCapitalize.get(): # Check if the user wants capitalization
                        for token in tokens: # Capitalize all words
                            if len(token) > 0: # To prevent IndexError
                                capitalizedToken = token.replace(token[0], token[0].upper(), 1) # In this case, capitalize() could potentially affect unwanted characters
                                newFileName = newFileName.replace(token, capitalizedToken)
                    else: # Set to lower case
                        for token in tokens: # All tokens
                            if len(token) > 0: # To prevent IndexError
                                capitalizedToken = token.replace(token[0], token[0].lower(), 1) # In this case, lower() could potentially affect unwanted characters
                                newFileName = newFileName.replace(token, capitalizedToken)
                    os.rename(os.path.join(path, originalFileName), os.path.join(path, newFileName))
        except PermissionError:
            consoleLogLabel["text"] = "Permission error: rename failed."
            consoleLogLabel["foreground"] = "red"
            return
        except IndexError:
            consoleLogLabel["text"] = "Index error: rename failed."
            consoleLogLabel["foreground"] = "red"
            return
        consoleLogLabel["text"] = "Renamed all files within directory."
        consoleLogLabel["foreground"] = "green"

def openRepository():
    webbrowser.open_new("https://gitlab.com/matheusvilano/delimiter-batch-processor") # Open my repository's main page

# Widgets

# Row 0

versionLabel = tk.Label(app, text="For more information, visit the GitLab repository:", justify=LEFT)
versionLabel.grid(row=0, column=0, columnspan=4)

repoButton = tk.Button(app, text="Repository", width=18, height=1, justify=LEFT, command=openRepository, border=borderSize, background=backgroundColour, foreground=foregroundColour)
repoButton.grid(row=0, column=4, columnspan=2, padx=paddingSize, pady=paddingSize)

# Row 1

directoryLabel = tk.Label(app, text="Directory: ")
directoryLabel.grid(row=1, column=0, sticky="ew")

directoryEntry = tk.Entry(app, width=35, border=borderSize)
directoryEntry.grid(row=1, column=1, columnspan=3, sticky="w")

directoryBrowseButton = tk.Button(app, text="Browse", width=18, justify=CENTER, command=browseDirectory, border=borderSize, background=backgroundColour, foreground=foregroundColour)
directoryBrowseButton.grid(row=1, column=4, columnspan=2, padx=paddingSize, pady=paddingSize)

# Row 2

wildcardsLabel = tk.Label(app, text="Wildcards: ")
wildcardsLabel.grid(row=2, column=0, padx=paddingSize, pady=paddingSize)

wildcardsSetLabel = tk.Label(app, text="No wildcards.", foreground="blue")
wildcardsSetLabel.grid(row=2, column=1, sticky="ew")

wildcardsEntry = tk.Entry(app, textvariable=wildcardText, width=7, justify=CENTER, border=borderSize)
wildcardsEntry.grid(row=2, column=2, columnspan=2)
wildcardsEntry.bind("<Return>", onWildcardShortcutPressed)
wildcardsEntry.bind("<Escape>", onWildcardShortcutPressed)
wildcardText.trace('w', lambda *args: checkWildcardCharacterCount()) # Limit character count

wildcardsAddButton = tk.Button(app, text="Add", width=7, justify=CENTER, command=addWildcard, border=borderSize, background=backgroundColour, foreground=foregroundColour)
wildcardsAddButton.grid(row=2, column=4, padx=paddingSize, pady=paddingSize)

wildcardsRemoveButton = tk.Button(app, text="Remove", width=7, justify=CENTER, command=removeWildcard, border=borderSize, background=backgroundColour, foreground=foregroundColour)
wildcardsRemoveButton.grid(row=2, column=5, padx=paddingSize, pady=paddingSize)

# Row 3

capitalizationCheckbox = tk.Checkbutton(app, text="Capitalize?", justify=CENTER, variable=willCapitalize, border=borderSize)
capitalizationCheckbox.grid(row=3, column=0, sticky="ew")

consoleLogLabel = tk.Label(app, text="No logged messages.", foreground="blue", justify=CENTER, border=borderSize)
consoleLogLabel.grid(row=3, column=1, columnspan=3, sticky="ew")

renameFilesButton = tk.Button(app, text="Rename Files", width=18, height=1, justify=CENTER, command=renameFiles, border=borderSize, background=backgroundAltColour, foreground=foregroundAltColour)
renameFilesButton.grid(row=3, column=4, columnspan=2, padx=paddingSize, pady=paddingSize)

# Main
app.mainloop() # This actually starts the GUI app