# Delimiter Batch Processor

A tiny Python application that formats with either of these naming conventions: "Ab_Cd_Ef" or "ab_cd_ef". I personally use this to organize my sound effects libraries (personal and purchased).